#! /usr/bin/env python3
"""Provides the legacy clustering method for onset time retrieval."""

import numpy as np
from scipy.cluster import hierarchy as ch

def find_t0(time, data, height=0.003, plot=False, isTimeNormalized=False, dgrm_args={}):
    """Finds the onset time by legacy algorithm. Not robust on new data.

    Input parameters:
        time: numpy vector of times of datapoints (in hours)
        data: numpy vector of fluorescence intensity
        height: threshold at which to cut cluster tree
        plot: boolean indicating whether to plot a dedrogram
        isTimeNormalized: boolean indicating wheter `time` is normalized already
        dgrm_args: arguments to be passed to dendrogram function

    The return value is a dictionary with these fields:
        t0: the onset time found
        Z: the cluster tree returned by `linkage`
        dgrm: return value of `dendrogram` function (or `None` if `plot==False`)
    """

    # Normalized time (in hours/50)
    if not isTimeNormalized:
        tn = (time - time.min()) / (time.max() - time.min()) / 50
    else:
        tn = time

    # Normalized data
    dn = (data - data.min()) / (data.max() - data.min())

    # Gradient of normalized data
    gradn = np.gradient(dn)

    # Combine to observation matrix
    tab = np.array([tn, dn, gradn]).T

    # Perform clustering
    Z = ch.linkage(tab, method='single', metric='cityblock')

    # Find last time point in initial cluster
    ct = ch.cut_tree(Z, height=height).flatten()
    t0 = time[ct == ct[0]][-1]

    # Plot dendrogram, if desired
    if plot:
        if len(dgrm_args) == 0:
            dgrm_args = {'color_threshold': height}
        elif 'color_threshold' in dgrm_args.keys() and dgrm_args['color_threshold'] < 0:
            dgrm_args['color_threshold'] = height
        dgrm = ch.dendrogram(Z, **dgrm_args)
    else:
        dgrm = None

    return {'t0': t0, 'Z': Z, 'dgrm': dgrm}
