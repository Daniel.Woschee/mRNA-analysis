{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Onset time retrieval by clustering (legacy algorithm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview\n",
    "This notebook retrieves the onset time $t_0$ of the fluorescence signals by clustering.\n",
    "\n",
    "The main idea is to identify the cluster of points with constantly low fluorescence intensity before the onset, from now on referred to as _initial cluster_. $t_0$ is the time of the last datapoint in the initial cluster.\n",
    "\n",
    "The following sections describe how the data is prepared for clustering, what clustering algorithm is performed, and how the initial cluster is found in the cluster data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data preparation\n",
    "The dimensions in which the clusters are calculated are the normalized flourescence intensity, its derivative and the normalized time.\n",
    "\n",
    "From the fluorescence intensity $D=\\{d_i\\mid i=0,\\ldots,N-1\\}$, the normalized fluorescence intensity $\\hat{D}=\\{\\hat{d}_i\\}$ is given by\n",
    "$$\n",
    "\\hat{d}_i = \\frac{d_i - \\min(D)}{\\max(D)-\\min(D)}\\text{.}\n",
    "$$\n",
    "\n",
    "The derivative $\\Delta=\\{\\delta_i\\}$ of $\\hat{D}$ is given by\n",
    "$$\n",
    "\\delta_i =\n",
    "\\begin{cases}\n",
    "\\delta_1 &\\text{if $i=0$} \\\\\n",
    "\\max\\left(\\frac{\\hat{d}_i - \\hat{d}_{i-1}}{t_i - t_{i-1}}, 0\\right) &\\text{else}\n",
    "\\end{cases}\n",
    "\\text{.}\n",
    "$$\n",
    "The derivative is computed with respect to only the previous time point for finding the onset more accurately, and normalized by time to account for non-equidistant time steps.\n",
    "For the latter reason, the approach of `numpy.gradient` was not used, either.\n",
    "\n",
    "The derivative of the first point is calculated with respect to the subsequent point because there is no previous point.\n",
    "Negative derivatives are set to 0 because the expected behaviour of the traces in the given region is monotonously increasing, and negative values are merely noise.\n",
    "\n",
    "The normalized time $\\hat{T}=\\{\\hat{t}_i\\mid i=0,\\ldots,N-1\\}$ is obtained from the time $T=\\{t_i\\mid i=0,\\ldots,N-1\\}$ by\n",
    "$$\n",
    "\\hat{t}_i = \\frac{1}{\\tau} \\frac{t_i - \\min(T)}{\\max(T)-\\min(T)} \\text{,}\n",
    "$$\n",
    "with the scaling factor $\\tau=20$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clustering algorithm\n",
    "The clustering is performed using a hierarchical clustering algorithm. The functionality is provided by the function `scipy.cluster.hierarchy.linkage`, whose documentation is [here](https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html).\n",
    "\n",
    "The linkage method that is used is `single`. According to the documentation, the distance $d(X,Y)$ between two clusters $X$ and $Y$ is\n",
    "$$\n",
    "d(X,Y) = \\min_{i,j}(\\mathrm{dist}(x_i, y_j)) \\text{,}\n",
    "$$\n",
    "where $x_i \\in X, i=1,\\ldots,|X|$ and $y_j \\in Y, j=1,\\ldots,|Y|$.\n",
    "Or more intuitively, the distance between two clusters is the distance between their two points with the smallest distance.\n",
    "\n",
    "The metric used is the `cityblock` metric, also called Manhattan metric.\n",
    "It is the sum of the absolute values of the dimensions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initial cluster retrieval\n",
    "The initial cluster consists of all clusters containing the first time point that have a cluster height below a threshold.\n",
    "The threshold is currently 0.025."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.cluster import hierarchy as ch\n",
    "from scipy.io import loadmat\n",
    "import os\n",
    "import pandas as pd\n",
    "#pd.set_option('display.max_columns', None)\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.backends.backend_pdf import PdfPages\n",
    "%matplotlib inline\n",
    "import matplotlib as mpl\n",
    "mpl.rcParams['pdf.fonttype'] = 42 # Make fonts editable by Adobe Illustrator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data\n",
    "<div class=\"alert alert-block alert-warning\">Execute only **one** of the following cells!  \n",
    "Otherwise, only the last cell called will take effect.\n",
    "</div>\n",
    "\n",
    "There are nine datasets available. Some of them can were measured under the same conditions and may be combined.\n",
    "* K1\n",
    "* K3\n",
    "* K5\n",
    "* lipo\n",
    "  * lipo11\n",
    "  * lipo12\n",
    "* lipo2\n",
    "  * lipo21\n",
    "  * lipo22\n",
    "* DDC\n",
    "  * DDC1\n",
    "  * DDC2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K1\n",
    "data_tab = pd.read_excel('20170316_K1_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K1_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K3\n",
    "data_tab = pd.read_excel('20170316_K3_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K3_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K5\n",
    "data_tab = pd.read_excel('20170316_K5_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K5_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo11\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo11_gfp'\n",
    "time = data_tab['times'][0][0].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo12\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo12_gfp'\n",
    "time = data_tab['times'][0][1].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][1]\n",
    "\n",
    "# Delete trace #316 for comparability with fitting\n",
    "egfp = np.delete(egfp, 316, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo21\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo21_gfp'\n",
    "time = data_tab['times'][0][2].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo22\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo22_gfp'\n",
    "time = data_tab['times'][0][3].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from DDC1\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_DDC1_gfp'\n",
    "time = data_tab['times'][0][4].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][4]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from DDC2\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_DDC2_gfp'\n",
    "time = data_tab['times'][0][5].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getTimeStamp():\n",
    "    \"\"\"Returns a human-readable string representation of the current time\"\"\"\n",
    "    import time\n",
    "    from datetime import datetime\n",
    "    return datetime.now().strftime(\"%Y-%m-%d–%H%M%S\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getOutpath():\n",
    "    \"\"\"Returns (and creates, if necessary) the path to a directory called “out” inside the current directory.\"\"\"\n",
    "    import os\n",
    "    outpath = os.path.join(os.getcwd(), 'out')\n",
    "    if not os.path.isdir(outpath) and os.path.lexists(outpath):\n",
    "        os.path.mkdir(outpath)\n",
    "    return outpath"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def get_Z(time, data, isTimeNormalized=False, time_scale=20):\n",
    "    \"\"\"Finds the onset time by legacy algorithm. Not robust on new data.\n",
    "\n",
    "    Input parameters:\n",
    "        time: numpy vector of times of datapoints (in hours)\n",
    "        data: numpy vector of fluorescence intensity\n",
    "        isTimeNormalized: boolean indicating wheter `time` is normalized already\n",
    "        time_scale: scaling factor by which to divide the normalized time\n",
    "\n",
    "    Return value:\n",
    "        Z: the cluster tree returned by `linkage`\n",
    "    \"\"\"\n",
    "\n",
    "    # Normalized time (in hours/`time_scale`)\n",
    "    if not isTimeNormalized:\n",
    "        tn = (time - time.min()) / (time.max() - time.min()) / time_scale\n",
    "    else:\n",
    "        tn = time\n",
    "\n",
    "    # Normalized data\n",
    "    dn = (data - data.min()) / (data.max() - data.min())\n",
    "\n",
    "    # Gradient of normalized data\n",
    "    gradn = np.empty(np.shape(dn))\n",
    "    gradn[1:] = (dn[1:] - dn[:-1]) / (time[1:] - time[:-1])\n",
    "    gradn[0] = gradn[1]\n",
    "    gradn[gradn < 0] = 0\n",
    "\n",
    "    # Combine to observation matrix\n",
    "    tab = np.array([tn, dn, gradn]).T\n",
    "\n",
    "    # Perform clustering\n",
    "    Z = ch.linkage(tab, method='single', metric='cityblock')\n",
    "\n",
    "    return Z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def get_t0(Z, time, height=0.025):\n",
    "    \"\"\"Finds the onset time and the clusters before it by the legacy algorithm\n",
    "\n",
    "    Input parameters:\n",
    "        Z: the cluster tree returned by `linkage`\n",
    "        time: numpy vector of times of datapoints (in hours)\n",
    "        height: threshold at which to cut cluster tree\n",
    "\n",
    "    Return value:\n",
    "        t0: the onset time found\n",
    "    \"\"\"\n",
    "    # Find last time point in initial cluster\n",
    "    ct = ch.cut_tree(Z, height=height).flatten()\n",
    "    t0 = time[ct == ct[0]][-1]\n",
    "\n",
    "    return t0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getBranchTime(b, Z, time):\n",
    "    \"\"\"Returns the time corresponding to a cluster node\n",
    "        b: node index (zero-based)\n",
    "        Z: cluster tree as returned from scipy.cluster.hierarchy.linkage\n",
    "        time: the time vector of measurements\"\"\"\n",
    "    nLeaves = time.size\n",
    "    b = int(b)\n",
    "    if b < nLeaves:\n",
    "        return time[b]\n",
    "    else:\n",
    "        b -= nLeaves\n",
    "        t1 = getBranchTime(Z[b,0], Z, time)\n",
    "        t2 = getBranchTime(Z[b,1], Z, time)\n",
    "        return (t1 + t2) / 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def climb_up(c, Z):\n",
    "    \"\"\"Climbs cluster hierarchy up by one step.\n",
    "\n",
    "    Input parameters:\n",
    "        c: cluster to start from\n",
    "        Z: cluster tree (from `linkage`)\n",
    "\n",
    "    Returns:\n",
    "        index of the cluster containing cluster `c`, or empty array if no cluster found\n",
    "    \"\"\"\n",
    "    return np.flatnonzero(np.any(Z[:,:2] == c, axis=1)) + Z.shape[0] + 1\n",
    "\n",
    "def climb_down(c, Z):\n",
    "    \"\"\"Climbs cluster hierarchy down by one step.\n",
    "\n",
    "    Input parameters:\n",
    "        c: cluster to start from\n",
    "        Z: cluster tree (from `linkage`)\n",
    "\n",
    "    Returns:\n",
    "        array of indices of child clusters, or empty array if no child clusters found\n",
    "    \"\"\"\n",
    "    c -= Z.shape[0] + 1\n",
    "    if c < 0:\n",
    "        return np.empty((0), dtype=np.intp)\n",
    "    else:\n",
    "        return Z[c,:2].astype(np.intp)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def initial_clusters(Z, height=0.025):\n",
    "    \"\"\"Returns a boolean array indicating nodes belonging to initial cluster\n",
    "\n",
    "    Input parameters:\n",
    "        Z: cluster tree (from `linkage`)\n",
    "        height: maximum height of initial cluster\n",
    "\n",
    "    Returns:\n",
    "        boolean array of size `Z.shape[0] * 2 + 1`, such that the i-th value is\n",
    "        `True` iff the i-th cluster belongs to the initial cluster\n",
    "    \"\"\"\n",
    "    # Initialize variables\n",
    "    nLeaves = Z.shape[0] + 1\n",
    "    isInitial = np.zeros((nLeaves * 2 - 1), dtype=np.bool_)\n",
    "    maxInitial = 0\n",
    "\n",
    "    # Climb up to highest node in initial cluster\n",
    "    while True:\n",
    "        newMax = climb_up(maxInitial, Z)\n",
    "        if Z[newMax - nLeaves, 2] > height:\n",
    "            break\n",
    "        elif newMax.size == 0:\n",
    "            break\n",
    "        else:\n",
    "            maxInitial = newMax\n",
    "\n",
    "    # Recursively set entries for all nodes in initial cluster to `True`\n",
    "    inits = maxInitial.tolist()\n",
    "    for i in inits:\n",
    "        isInitial[i] = True\n",
    "        inits.extend(climb_down(i, Z).tolist())\n",
    "\n",
    "    return isInitial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run the analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Get onset times by clustering\n",
    "nTraces = np.shape(egfp)[1]\n",
    "ts = getTimeStamp()\n",
    "t0s = np.zeros((nTraces))\n",
    "nLeaves = time.size\n",
    "height = 0.025\n",
    "\n",
    "# Find t0 and plot to PDF\n",
    "with PdfPages(os.path.join(getOutpath(),\n",
    "    '{:s}_ONSET_CLUSTER_{}.pdf'.format(ts, data_label))) as pdf:\n",
    "\n",
    "    for iTr in range(nTraces):\n",
    "        # Find t0 by clustering\n",
    "        #res = fint_t0(time, egfp[:,iTr])\n",
    "        #Z = res['Z']\n",
    "        #t0 = res['t0']\n",
    "        Z = get_Z(time, egfp[:,iTr])\n",
    "        t0 = get_t0(Z, time, height=height)\n",
    "\n",
    "        # Write t0 to list\n",
    "        t0s[iTr] = t0\n",
    "\n",
    "        # Write array indicating initial cluster\n",
    "        isInitial = initial_clusters(Z, height=height)\n",
    "\n",
    "        # Create figure to plot to\n",
    "        fig = plt.figure()\n",
    "        ax1 = fig.add_subplot(2, 1, 1)\n",
    "        ax2 = fig.add_subplot(2, 1, 2, sharex=ax1)\n",
    "        fig.subplots_adjust(hspace=0, wspace=0)\n",
    "\n",
    "        # Indicate onset time\n",
    "        ax1.axvline(t0, color='r')\n",
    "        ax2.axvline(t0, color='r')\n",
    "\n",
    "        # Plot trace with t0\n",
    "        ax1.plot(time, egfp[:,iTr], '-k')\n",
    "\n",
    "        # Plot vertical lines below graph\n",
    "        for t in range(time.size):\n",
    "            if isInitial[t]:\n",
    "                clr = 'm'\n",
    "            else:\n",
    "                clr = 'b'\n",
    "            ax1.plot([time[t], time[t]], [egfp[t,iTr], 0],\n",
    "                     '-', color=clr, linewidth=.5, clip_on=False)\n",
    "            ax1.plot(time[t], egfp[t,iTr],\n",
    "                    'o', color=clr, markersize=2, clip_on=False)\n",
    "\n",
    "        # Plot custom dendrogram because built-in dendrogram does\n",
    "        # not allow for custom ordering of leaves\n",
    "        for z in range(np.shape(Z)[0]):\n",
    "            b1, b2 = (Z[z,0], Z[z,1]) if Z[z,0] < Z[z,1] \\\n",
    "                else (Z[z,1], Z[z,0])\n",
    "            b1 = int(b1)\n",
    "            b2 = int(b2)\n",
    "            h1 = 0 if b1 < nLeaves else Z[b1-nLeaves,2]\n",
    "            h2 = 0 if b2 < nLeaves else Z[b2-nLeaves,2]\n",
    "            t1 = getBranchTime(b1, Z, time)\n",
    "            t2 = getBranchTime(b2, Z, time)\n",
    "\n",
    "            node = np.empty((4, 2))\n",
    "            node[0,:] = [t1, h1]\n",
    "            node[1,:] = [t1, Z[z,2]]\n",
    "            node[2,:] = [t2, Z[z,2]]\n",
    "            node[3,:] = [t2, h2]\n",
    "\n",
    "            if isInitial[z + nLeaves]:\n",
    "                clr = 'm'\n",
    "            else:\n",
    "                clr = 'b'\n",
    "            ax2.plot(node[:,0], node[:,1], '-',\n",
    "                     color=clr, linewidth=.5)\n",
    "\n",
    "        # Format figure\n",
    "        ax1.tick_params('x', bottom=False, labelbottom=False)\n",
    "        ax1.set_ylim(bottom=0)\n",
    "\n",
    "        ax2.invert_yaxis()\n",
    "        #ax2.set_ylim(top=0)\n",
    "        ax2.set_yscale('log')\n",
    "        ax1.set_axisbelow(True)\n",
    "        ax2.set_axisbelow(True)\n",
    "\n",
    "        ax2.set_xlabel('Time [h]')\n",
    "        ax1.set_ylabel('Fluorescence [a.u.]')\n",
    "        ax2.set_ylabel('Distance [a.u.]')\n",
    "        fig.suptitle('Trace {:03d}'.format(iTr))\n",
    "\n",
    "        plt.show(fig)\n",
    "        pdf.savefig(fig)\n",
    "        plt.close(fig)\n",
    "\n",
    "        # Reduce computation time while developing\n",
    "        #if iTr == 10:\n",
    "        #    break\n",
    "\n",
    "# Save times to numpy array\n",
    "np.save(\"{}_t0_cluster\".format(data_label), t0s)\n",
    "\n",
    "# Write t0 list to CSV\n",
    "pd.DataFrame(t0s).to_csv(\n",
    "    os.path.join(getOutpath(),\n",
    "         '{:s}_ONSET_CLUSTER_{}.csv'.format(ts, data_label)),\n",
    "    na_rep='NaN',\n",
    "    header=False,\n",
    "    index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Combine datasets of same conditions\n",
    "\n",
    "# Get data labels of datasets to be combined\n",
    "data_labels = ('20161006_lipo1', '20161006_lipo2', '20161006_DDC')\n",
    "suffix = '_gfp_t0_cluster'\n",
    "ext = '.npy'\n",
    "\n",
    "# Get files to be combined\n",
    "fns = {}\n",
    "for fn in [f for f in os.listdir('.') if os.path.isfile(f)]:\n",
    "    (root, ex) = os.path.splitext(fn)\n",
    "    if ex != ext or not root.endswith(suffix):\n",
    "        continue\n",
    "    for lbl in data_labels:\n",
    "        if root.startswith(lbl) and \\\n",
    "                len(root) == 1 + len(lbl) + len(suffix) and \\\n",
    "                root[len(lbl)].isdigit():\n",
    "            if lbl not in fns.keys():\n",
    "                fns[lbl] = []\n",
    "            fns[lbl].append(fn)\n",
    "\n",
    "# Combine files\n",
    "for lbl, infiles in fns.items():\n",
    "    infiles.sort()\n",
    "    outfile = lbl + suffix + ext\n",
    "    t0out = np.empty((0))\n",
    "    t0_mtime_max = 0;\n",
    "\n",
    "    # Load files\n",
    "    for infile in infiles:\n",
    "        t0_mtime_max = max(t0_mtime_max, os.path.getmtime(infile))\n",
    "        t0out = np.concatenate((t0out, np.load(infile).flatten()))\n",
    "\n",
    "    # Test if outfile is newer than newest infile\n",
    "    # if yes, quit this file\n",
    "    if os.path.isfile(outfile) and \\\n",
    "            os.path.getmtime(outfile) > t0_mtime_max:\n",
    "        print('Quitting new file: ' + outfile)\n",
    "        continue\n",
    "\n",
    "    # Write combined list\n",
    "    print('Writing: ' + outfile)\n",
    "    np.save(outfile, t0out)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Test differences between time points\n",
    "for t, d in zip(time, np.diff(time)):\n",
    "    print(\"t={:6.3f}, d={:4.3f}\".format(t,d))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
