{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Onset time retrieval by clustering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview\n",
    "This notebook retrieves the onset time $t_0$ of the fluorescence signals by clustering.\n",
    "\n",
    "The main idea is to identify the cluster of points with constantly low fluorescence intensity before the onset, from now on referred to as _initial cluster_. $t_0$ is the time of the last datapoint in the initial cluster.\n",
    "\n",
    "The following sections describe how the data is prepared for clustering, what clustering algorithm is performed, and how the initial cluster is found in the cluster data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data preparation\n",
    "The dimensions in which the clusters are calculated are the normalized flourescence intensity and its derivative. Former approaches had been using the normalized time, too, but it turned out that leaving the time out yields better results.\n",
    "\n",
    "From the fluorescence intensity $D=\\{d_i\\mid i=0,\\ldots,N-1\\}$, the normalized fluorescence intensity $\\hat{D}=\\{\\hat{d}_i\\}$ is given by\n",
    "$$\n",
    "\\hat{d}_i = \\frac{d_i - \\min(D)}{\\max(D)-\\min(D)}\\text{.}\n",
    "$$\n",
    "\n",
    "The derivative $\\Delta=\\{\\delta_i\\}$ of $\\hat{D}$ is given by\n",
    "$$\n",
    "\\delta_i =\n",
    "\\begin{cases}\n",
    "0 &\\text{if $i=0$} \\\\\n",
    "\\hat{d}_{i-1} - \\hat{d}_i &\\text{else}\n",
    "\\end{cases}\n",
    "\\text{.}\n",
    "$$\n",
    "The function `numpy.gradient` was not used here by intention because otherwise, the derivative at the last datapoint before onset is biased so that it wouldn’t be counted to the initial cluster."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Clustering algorithm\n",
    "The clustering is performed using a hierarchical clustering algorithm. The functionality is provided by the function `scipy.cluster.hierarchy.linkage`, whose documentation is [here](https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html).\n",
    "\n",
    "The linkage method that yielded the best results is `median`, also called WPGMC algorithm. According to the documentation, the distance $d(s,t)$ between two clusters $s$ and $t$ is\n",
    "$$\n",
    "d(s,t) = \\lVert c_s - c_t \\rVert_2 \\text{,}\n",
    "$$\n",
    "with $c_s$ and $c_t$ being the centroids of $s$ and $t$, respectively. The centroid of a cluster constructed by merging $s$ and $t$ is given by the average of $c_s$ and $c_t$.\n",
    "\n",
    "The `centroid` method gives similar results in most cases, but the `median` method appears to be more robust with respect to peaks before the onset, where it tends to include all datapoints into the initial cluster. Such cases are indicated by $t_0 = \\mathtt{NaN}$.\n",
    "\n",
    "Since the `median` method requires the euclidean distance measure, the latter is used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cluster analysis\n",
    "The linkage function returns a tree `Z` of nodes, whose leaves are the data points $D$. The function `get_to` starts at the first leaf $d_0$, climbs up in `Z` to the next node (_parent_), and adds the parent and all its children to the initial cluster until all three stop conditions are fulfilled.\n",
    "\n",
    "The first stop condition is the change in maximum height difference. The linkage function assigns a height value to each non-leaf node. The height value indicates the distance between the children of the node.  \n",
    "The maximum height difference is the largest difference between the height of a node and any of its children. Since the nodes before onset have small distances, they have similar heights. When adding a new node to the initial cluster, its height will be merely slightly larger than of its children if they are before the onset. If, however, a child of the new node is after the onset, the height of the node is much larger than of the nodes in the initial cluster.  \n",
    "Therefore, the change of the maximum height in the initial cluster is checked when attempting to add a new node to the initial cluster. If the new maximum height difference is larger than twice its previous value, the first stop condition is fulfilled. The factor two is an educated guess that has performed well.  \n",
    "This is the main stop condition. However, other stop conditions are needed for a robust algorithm.\n",
    "\n",
    "The second stop condition is the number of nodes in the initial cluster. These can be leaves or non-leaves. The second stop condition is fulfilled if at least 15 nodes are inside the initial cluster.  \n",
    "This criterion is necessary to ensure that the analysis does not return during the first few iterations, because then the height differences can be so small that any new node would exceed the limit. Therefore, the second stop condition can be understood as initial aid.  \n",
    "A drawback of this criterion is that very early onset times cannot be detected because the algorithm is forced to ignore the large change in maximum height difference. Traces with such early onsets, however, have not been encountered yet.\n",
    "\n",
    "The third stop condition is fulfilled if there are no _orphan_ nodes. An orphan is a leaf that has no parent in the initial cluster (and, consequently, does not belong to the initial cluster itself), but that is at an earlier time than the last leaf in the initial cluster.  \n",
    "When an orphan is detected, the algorithm is forced to climb up `Z` until the orphan belongs to the initial cluster. This increases the maximum height difference, which can be both an advantage or a disadvantage, depending on the single trace.\n",
    "\n",
    "When all three stop conditions are fulfilled, no nodes are added to the initial cluster any more, and the onset time $t_0$ is set to the time corresponding to the last leaf in the initial cluster.\n",
    "\n",
    "In the figure, `Z` is depicted as a tree in the lower axes. Vertical lines connect the leaves to the corresponding points on the trace. Nodes in the initial cluster are drawn in magenta, whereas all other nodes in `Z` are drawn in blue. $t_0$ is indicated by a red vertical line. If $t_0 = \\mathtt{NaN}$, there is no red line, and all nodes are drawn in blue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.cluster import hierarchy as ch\n",
    "from scipy.io import loadmat\n",
    "import os\n",
    "import pandas as pd\n",
    "#pd.set_option('display.max_columns', None)\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.backends.backend_pdf import PdfPages\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load data\n",
    "<div class=\"alert alert-block alert-warning\">Execute only **one** of the following cells!  \n",
    "Otherwise, only the last cell called will take effect.\n",
    "</div>\n",
    "\n",
    "There are nine datasets available. Some of them can were measured under the same conditions and may be combined.\n",
    "* K1\n",
    "* K3\n",
    "* K5\n",
    "* lipo\n",
    "  * lipo11\n",
    "  * lipo12\n",
    "* lipo2\n",
    "  * lipo21\n",
    "  * lipo22\n",
    "* DDC\n",
    "  * DDC1\n",
    "  * DDC2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K1\n",
    "data_tab = pd.read_excel('20170316_K1_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K1_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K3\n",
    "data_tab = pd.read_excel('20170316_K3_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K3_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from K5\n",
    "data_tab = pd.read_excel('20170316_K5_gfp_timecorr.xlsx', index_col=None,header=None)\n",
    "data_label = '20170316_K5_gfp'\n",
    "time = data_tab.iloc[:,0].values / 3600\n",
    "egfp = data_tab.iloc[:,1:].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo11\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo11_gfp'\n",
    "time = data_tab['times'][0][0].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo12\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo12_gfp'\n",
    "time = data_tab['times'][0][1].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][1]\n",
    "\n",
    "# Delete trace #316 for comparability with fitting\n",
    "egfp = np.delete(egfp, 316, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo21\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo21_gfp'\n",
    "time = data_tab['times'][0][2].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from lipo22\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_lipo22_gfp'\n",
    "time = data_tab['times'][0][3].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract the data from DDC1\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_DDC1_gfp'\n",
    "time = data_tab['times'][0][4].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][4]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Extract the data from DDC2\n",
    "data_tab = loadmat('egfp.mat')\n",
    "data_label = '20161006_DDC2_gfp'\n",
    "time = data_tab['times'][0][5].flatten() / 3600\n",
    "egfp = data_tab['egfp'][0][5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getTimeStamp():\n",
    "    \"\"\"Returns a human-readable string representation of the current time\"\"\"\n",
    "    import time\n",
    "    from datetime import datetime\n",
    "    return datetime.now().strftime(\"%Y-%m-%d–%H%M%S\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getOutpath():\n",
    "    \"\"\"Returns (and creates, if necessary) the path to a directory called “out” inside the current directory.\"\"\"\n",
    "    import os\n",
    "    outpath = os.path.join(os.getcwd(), 'out')\n",
    "    if not os.path.isdir(outpath) and os.path.lexsits(outpath):\n",
    "        os.path.mkdir(outpath)\n",
    "    return outpath"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def get_Z(time, data, isTimeNormalized=False):\n",
    "    \"\"\"Clusters the trace and returns linkage tree\"\"\"\n",
    "    # Normalized time (in hours/50)\n",
    "    #if not isTimeNormalized:\n",
    "    #    tn = (time - time.min()) / (time.max() - time.min()) / 50\n",
    "    #else:\n",
    "    #    tn = time\n",
    "\n",
    "    # Normalized data\n",
    "    dn = (data - data.min()) / (data.max() - data.min())\n",
    "\n",
    "    # Gradient of normalized data\n",
    "    #gradn = np.gradient(dn)\n",
    "    nPoints = np.size(data)\n",
    "    gradn = np.empty(nPoints)\n",
    "    gradn[0] = 0\n",
    "    gradn[1:] = dn[1:] - dn[:-1]\n",
    "\n",
    "    # Combine to observation matrix\n",
    "    tab = np.array([dn, gradn]).T\n",
    "    #tab = np.array([tn, dn, gradn]).T\n",
    "    #tab = np.array([tn, dn]).T\n",
    "\n",
    "    # Perform clustering\n",
    "    Z = ch.linkage(tab, method='median', metric='euclidean')\n",
    "\n",
    "    return Z"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def get_t0(Z, time):\n",
    "    \"\"\"Find the onset time and the clusters before it\"\"\"\n",
    "    # Number of leaves\n",
    "    nLeaves = np.size(time)\n",
    "\n",
    "    # Clusters found before onset\n",
    "    before_onset = {0}\n",
    "\n",
    "    def parent_in_Z(child):\n",
    "        \"\"\"Find parent of cluster with label `child`\"\"\"\n",
    "        return nLeaves + np.flatnonzero(np.any(\n",
    "            Z[:,:2] == child, axis=1)).astype(np.intp)\n",
    "\n",
    "    def children_in_Z(parent):\n",
    "        \"\"\"Find the child clusters of `parent`\"\"\"\n",
    "        if parent < nLeaves:\n",
    "            return []\n",
    "        return Z[parent-nLeaves,:2].flatten().astype(np.intp)\n",
    "\n",
    "    def max_diff_h(root, ch=-1, mdh_ch=None, descendants=set()):\n",
    "        \"\"\"Get maximum parent-child difference below `root`\n",
    "        Arguments:\n",
    "            root: index of largest cluster in which to search\n",
    "            ch: child of `root` with known height difference\n",
    "                (-1 if no child given)\n",
    "            mdh_ch: height difference of `ch`\n",
    "            descendants: clusters below `root` are added to `descendants`\n",
    "        Returns:\n",
    "            Maximum height difference\n",
    "            Set of clusters below `root`\"\"\"\n",
    "        # Leaves have height difference 0\n",
    "        if root < nLeaves:\n",
    "            return 0, descendants\n",
    "\n",
    "        # Get children of `root`\n",
    "        ch1, ch2 = children_in_Z(root)\n",
    "\n",
    "        # Convert cluster index to row in `Z`\n",
    "        #root -= nLeaves\n",
    "        root_idx = root - nLeaves\n",
    "\n",
    "        # Get height differences between `root` and its children\n",
    "        # (Height differences to leaves are 0)\n",
    "        dh1, dh2 = 0, 0\n",
    "        if ch1 >= nLeaves:\n",
    "            dh1 = float(Z[root_idx,2] - Z[ch1-nLeaves,2])\n",
    "        if ch2 >= nLeaves:\n",
    "            dh2 = float(Z[root_idx,2] - Z[ch2-nLeaves,2])\n",
    "\n",
    "        # If a maximum height difference for a child is given,\n",
    "        # use it instead of recalculating it.\n",
    "        # Register newly detected children in `descendants`.\n",
    "        if ch1 == ch:\n",
    "            mdh1 = mdh_ch\n",
    "        else:\n",
    "            mdh1 = max_diff_h(ch1, descendants)[0]\n",
    "            descendants.add(ch1)\n",
    "        if ch2 == ch:\n",
    "            mdh2 = mdh_ch\n",
    "        else:\n",
    "            mdh2 = max_diff_h(ch2, descendants)[0]\n",
    "            descendants.add(ch2)\n",
    "\n",
    "        # Return largest height difference below `root`\n",
    "        return (np.max([dh1, dh2, mdh1, mdh2]), descendants)\n",
    "\n",
    "\n",
    "    # Initialize variables for the loop:\n",
    "    # Indices of parent clusters during loop\n",
    "    i, i_new = 0, 0\n",
    "\n",
    "    # Maximum height difference below the current parent cluster\n",
    "    mdh, mdh_new = 0, 0\n",
    "\n",
    "    # Buffer for new clusters to be added to `before_onset` cluster\n",
    "    before_onset_new = set()\n",
    "\n",
    "    # Number of clusters encountered below current parent\n",
    "    n_clusters = 0\n",
    "\n",
    "    # Minimum number of clusters and maximum height factor\n",
    "    # (used for defining stopping criteria)\n",
    "    MIN_CLUSTERS = 15\n",
    "    MAX_HEIGHT_FACTOR = 2\n",
    "\n",
    "    # Indicator array for “orphans” (=leaves that are before the\n",
    "    # onset but are not counted to the cluster yet)\n",
    "    orphans = np.ones(nLeaves, dtype=np.bool_)\n",
    "\n",
    "    # Control variables for loop\n",
    "    hasOrphans = False\n",
    "    climbUp = True\n",
    "\n",
    "    # Walk from first point upwards until all stop conditions hold\n",
    "    # There are three stop conditions:\n",
    "    # * height difference: must not be larger than twice the\n",
    "    #   largest height difference encountered before\n",
    "    # * number of clusters: at least `MIN_CLUSTERS` must be found\n",
    "    #   before onset\n",
    "    # * orphans (non-cluster leaves before a cluster leaf): all\n",
    "    #   orphans must be absorbed into the cluster\n",
    "    while climbUp or hasOrphans:\n",
    "        # Accept *_new values\n",
    "        i = i_new\n",
    "        mdh = mdh_new\n",
    "        before_onset |= before_onset_new\n",
    "        before_onset.add(i_new)\n",
    "\n",
    "        # Go one step up and find new maximum height difference\n",
    "        i_new = parent_in_Z(i)\n",
    "        if np.size(i_new) != 1:\n",
    "            # Root cluster found; return\n",
    "            return np.NaN, np.array(sorted(before_onset), dtype=np.intp)\n",
    "        i_new = int(i_new)\n",
    "        mdh_new, before_onset_new = max_diff_h(i_new, ch=i, mdh_ch=mdh)\n",
    "\n",
    "        # Check for height difference condition\n",
    "        if i < nLeaves:\n",
    "            climbUp = True\n",
    "        elif Z[i_new-nLeaves,2] - Z[i-nLeaves,2] > MAX_HEIGHT_FACTOR * mdh:\n",
    "            climbUp = False\n",
    "        else:\n",
    "            climbUp = True\n",
    "\n",
    "        # Check number of clusters before onset\n",
    "        #n_clusters = int(Z[i-nLeaves,3]) if i >= nLeaves else 0\n",
    "        n_clusters = len(before_onset)\n",
    "        if n_clusters <= MIN_CLUSTERS:\n",
    "            climbUp = True\n",
    "\n",
    "        # Test for orphans (skip test if not necessary)\n",
    "        if not climbUp:\n",
    "            for c in before_onset:\n",
    "                if c >= nLeaves:\n",
    "                    continue\n",
    "                orphans[c] = False\n",
    "            if not np.any(orphans):\n",
    "                # All clusters are found; algorithm has failed\n",
    "                return np.NaN, np.array(sorted(before_onset), dtype=np.intp)\n",
    "            elif np.flatnonzero(orphans).min() < np.flatnonzero(np.logical_not(orphans)).max():\n",
    "                hasOrphans = True\n",
    "            else:\n",
    "                hasOrphans = False\n",
    "\n",
    "    # Finally, get the time of the largest point before onset\n",
    "    before_onset = np.array(sorted(before_onset), dtype=np.intp)\n",
    "    t0 = time[before_onset[before_onset<nLeaves][-1]]\n",
    "\n",
    "    # Return onset time and node indices\n",
    "    return t0, before_onset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def getBranchTime(b, Z, time):\n",
    "    \"\"\"Returns the time corresponding to a cluster node\n",
    "        b: node index (zero-based)\n",
    "        Z: cluster tree as returned from scipy.cluster.hierarchy.linkage\n",
    "        time: the time vector of measurements\"\"\"\n",
    "    nLeaves = np.size(time)\n",
    "    b = int(b)\n",
    "    if b < nLeaves:\n",
    "        return time[b]\n",
    "    else:\n",
    "        b -= nLeaves\n",
    "        t1 = getBranchTime(Z[b,0], Z, time)\n",
    "        t2 = getBranchTime(Z[b,1], Z, time)\n",
    "        return (t1 + t2) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run the analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Get onset times by clustering\n",
    "nTraces = np.shape(egfp)[1]\n",
    "ts = getTimeStamp()\n",
    "t0s = np.zeros((nTraces))\n",
    "\n",
    "# Find t0 and plot to PDF\n",
    "with PdfPages(os.path.join(getOutpath(),\n",
    "    '{:s}_ONSET_CLUSTER_{}.pdf'.format(ts, data_label))) as pdf:\n",
    "\n",
    "    for iTr in range(nTraces):\n",
    "        # Find t0 by clustering\n",
    "        #res = fint_t0(time, egfp[:,iTr])\n",
    "        #Z = res['Z']\n",
    "        #t0 = res['t0']\n",
    "        Z = get_Z(time, egfp[:,iTr])\n",
    "        t0, before_onset = get_t0(Z, time)\n",
    "\n",
    "\n",
    "        # Write t0 to list\n",
    "        t0s[iTr] = t0\n",
    "\n",
    "        # Create figure to plot to\n",
    "        fig = plt.figure()\n",
    "        ax1 = fig.add_subplot(2, 1, 1)\n",
    "        ax2 = fig.add_subplot(2, 1, 2, sharex=ax1)\n",
    "        fig.subplots_adjust(hspace=0, wspace=0)\n",
    "\n",
    "        # Indicate onset time\n",
    "        ax1.axvline(t0, color='r')\n",
    "        ax2.axvline(t0, color='r')\n",
    "\n",
    "        # Plot trace with t0\n",
    "        ax1.plot(time, egfp[:,iTr], '-k')\n",
    "\n",
    "        # Plot vertical lines below graph\n",
    "        for t in range(np.size(time)):\n",
    "            if time[t] <= t0:\n",
    "                clr = 'm'\n",
    "            else:\n",
    "                clr = 'b'\n",
    "            ax1.plot([time[t], time[t]], [egfp[t,iTr], 0],\n",
    "                     '-', color=clr, linewidth=.5)\n",
    "\n",
    "        # Plot custom dendrogram because built-in dendrogram does\n",
    "        # not allow for custom ordering of leaves\n",
    "        nLeaves = np.size(time)\n",
    "        for z in range(np.shape(Z)[0]):\n",
    "            b1, b2 = (Z[z,0], Z[z,1]) if Z[z,0] < Z[z,1] \\\n",
    "                else (Z[z,1], Z[z,0])\n",
    "            b1 = int(b1)\n",
    "            b2 = int(b2)\n",
    "            h1 = 0 if b1 < nLeaves else Z[b1-nLeaves,2]\n",
    "            h2 = 0 if b2 < nLeaves else Z[b2-nLeaves,2]\n",
    "            t1 = getBranchTime(b1, Z, time)\n",
    "            t2 = getBranchTime(b2, Z, time)\n",
    "            \n",
    "            node = np.empty((4, 2))\n",
    "            node[0,:] = [t1, h1]\n",
    "            node[1,:] = [t1, Z[z,2]]\n",
    "            node[2,:] = [t2, Z[z,2]]\n",
    "            node[3,:] = [t2, h2]\n",
    "\n",
    "            if t1 <= t0 and t2 <= t0:\n",
    "                clr = 'm'\n",
    "            else:\n",
    "                clr = 'b'\n",
    "            ax2.plot(node[:,0], node[:,1], '-',\n",
    "                     color=clr, linewidth=.5)\n",
    "\n",
    "        # Format figure\n",
    "        ax1.tick_params('x', bottom=False, labelbottom=False)\n",
    "        ax1.set_ylim(bottom=0)\n",
    "\n",
    "        ax2.invert_yaxis()\n",
    "        ax2.set_ylim(top=0)\n",
    "        ax1.set_axisbelow(True)\n",
    "        ax2.set_axisbelow(True)\n",
    "\n",
    "        ax2.set_xlabel('Time [h]')\n",
    "        ax1.set_ylabel('Fluorescence [a.u.]')\n",
    "        ax2.set_ylabel('Distance [a.u.]')\n",
    "        fig.suptitle('Trace {:03d}'.format(iTr))\n",
    "\n",
    "        plt.show(fig)\n",
    "        pdf.savefig(fig)\n",
    "        plt.close(fig)\n",
    "\n",
    "        # Reduce computation time while developing\n",
    "        #if iTr == 10:\n",
    "        #    break\n",
    "\n",
    "# Save times to numpy array\n",
    "np.save(\"{}_t0_cluster\".format(data_label), t0s)\n",
    "\n",
    "# Write t0 list to CSV\n",
    "pd.DataFrame(t0s).to_csv(\n",
    "    os.path.join(getOutpath(),\n",
    "         '{:s}_ONSET_CLUSTER_{}.csv'.format(ts, data_label)),\n",
    "    na_rep='NaN',\n",
    "    header=False,\n",
    "    index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Combine datasets of same conditions\n",
    "\n",
    "# Get data labels of datasets to be combined\n",
    "data_labels = ('20161006_lipo1', '20161006_lipo2', '20161006_DDC')\n",
    "suffix = '_gfp_t0_cluster'\n",
    "ext = '.npy'\n",
    "\n",
    "# Get files to be combined\n",
    "fns = {}\n",
    "for fn in [f for f in os.listdir('.') if os.path.isfile(f)]:\n",
    "    (root, ex) = os.path.splitext(fn)\n",
    "    if ex != ext or not root.endswith(suffix):\n",
    "        continue\n",
    "    for lbl in data_labels:\n",
    "        if root.startswith(lbl) and \\\n",
    "                len(root) == 1 + len(lbl) + len(suffix) and \\\n",
    "                root[len(lbl)].isdigit():\n",
    "            if lbl not in fns.keys():\n",
    "                fns[lbl] = []\n",
    "            fns[lbl].append(fn)\n",
    "\n",
    "# Combine files\n",
    "for lbl, infiles in fns.items():\n",
    "    infiles.sort()\n",
    "    outfile = lbl + suffix + ext\n",
    "    t0out = np.empty((0))\n",
    "    t0_mtime_max = 0;\n",
    "\n",
    "    # Load files\n",
    "    for infile in infiles:\n",
    "        t0_mtime_max = max(t0_mtime_max, os.path.getmtime(infile))\n",
    "        t0out = np.concatenate((t0out, np.load(infile).flatten()))\n",
    "\n",
    "    # Test if outfile is newer than newest infile\n",
    "    # if yes, quit this file\n",
    "    if os.path.isfile(outfile) and \\\n",
    "            os.path.getmtime(outfile) > t0_mtime_max:\n",
    "        print('Quitting new file: ' + outfile)\n",
    "        continue\n",
    "\n",
    "    # Write combined list\n",
    "    print('Writing: ' + outfile)\n",
    "    np.save(outfile, t0out)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
